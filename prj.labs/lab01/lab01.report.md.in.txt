## Работа 1. Исследование гамма-коррекции
автор: Петкин Д.В. 
дата: 25.02.2021

<!-- url: https://gitlab.com/danypetkin/petkin-d-v -->

### Задание
1. Сгенерировать серое тестовое изображение **I_1** в виде прямоугольника размером 768х60 пикселя с плавным изменение пикселей от черного к белому, одна градация серого занимает 3 пикселя по горизонтали.
2. Применить  к изображению **I_1** гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение **G_1** при помощи функци pow.
3. Применить  к изображению **I_1** гамма-коррекцию с коэффициентом из интервала 2.2-2.4 и получить изображение **G_2** при помощи прямого обращения к пикселям.
4. Показать визуализацию результатов в виде одного изображения (сверху вниз **I_1**, **G_1**, **G_2**).
5. Сделать замер времени обработки изображений в п.2 и п.3, результаты отфиксировать в отчете.

### Результаты

![](C:\polevoy\petkin_d_v\build\prj.labs\lab01\lab01.png)
Рис. 1. Результаты работы программы (сверху вниз **I_1**, **G_1**, **G_2**)

Время выполнения функции гамма-коррекции при помощи функци pow - 0.009
Время выполнения функции гамма-коррекции при помощи прямого обращения к пикселям - 0.014

### Текст программы

```cpp
#include <opencv2/opencv.hpp>
#include <cmath>
#include <ctime>

using namespace cv;
using namespace std;
void CopyIM(int first, int last, Mat ish, Mat top)
{
	for (size_t i = first; i < last; i++)
	{

		for (size_t j = 0; j < 768; j++)
		{
			ish.at<uchar>(i, j) = top.at<uchar>(i-first, j);
		}
	}
}
int main()
{
   Mat image(Mat::zeros(60, 768, 0));
   Mat tmp, I_1,G_2,G_1;

   for (int i = 0; i <= 255; i+=1)
   {
		   line(image, Point(i*3, 0), Point(i*3, 60), i, 3 );
   }


   //gammma pow
  
   image.convertTo(I_1, CV_64F);
   unsigned int start_time = clock();
   I_1 /= 255;
   pow(I_1, 2.3, tmp);
   tmp *= 255;   //преобразуем в одноканальное
   tmp.convertTo(G_1, 0);
   unsigned int end_time = clock();
   double search_time = double((end_time - start_time)) / CLOCKS_PER_SEC;

   //попиксельное
   G_2 = image;
   unsigned int start_ = clock();
   for (size_t i = 0; i < 60; i++)
   {
	  
	   for (size_t j = 0; j < 768; j++)
	   {  
		   G_2.at<uchar>(i,j) = (pow(double(G_2.at<uchar>(i, j))/255,2.3)*255);
	   }
   }
   unsigned int end_ = clock();
   double search_ = double((end_ - start_)) / CLOCKS_PER_SEC;

   //вывод 
   Mat res(Mat::zeros(180, 768, 0));
   CopyIM(0, 60, res, image);
   CopyIM(60, 120, res, G_1);
   CopyIM(120, 180, res, G_2);
  
   imshow("lab01", res);
   imwrite("lab01.png", res);
   cout << search_time << " " << search_;
   waitKey(0);
   return 0;
}
```

